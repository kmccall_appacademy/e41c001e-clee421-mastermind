class Code
  PEGS = { "R" => "Red",
           "G" => "Green",
           "B" => "Blue",
           "Y" => "Yellow",
           "O" => "Orange",
           "P" => "Purple"}

  attr_reader :pegs

  def initialize(arr_pegs)
    @pegs = arr_pegs
  end #initialize

  def self.parse(str)
    input_pegs = str.upcase.chars
    input_pegs.each {|peg| raise "Invalid color!" unless PEGS.key?(peg)}
    Code.new(input_pegs)
  end #parse

  def self.random
    c = PEGS.keys
    Code.new([ c[rand(PEGS.size)],
               c[rand(PEGS.size)],
               c[rand(PEGS.size)],
               c[rand(PEGS.size)] ])
  end

  def [](idx)
    pegs[idx]
  end #[]

  def exact_matches(other_code)
    cnt = 0
    for i in (0...pegs.length)
      cnt += 1 if pegs[i] == other_code[i]
    end
    cnt
  end #exact_matches

  def near_matches(other_code)
    this_hash = Hash.new(0)
    othe_hash = Hash.new(0)

    (0...pegs.length).each do |i|
      this_hash[ pegs[i] ] += 1
      othe_hash[ other_code[i] ] += 1
    end

    cnt = 0
    othe_hash.each do |k, v|
      if this_hash.key?(k)
        this_hash[k] < v ? cnt += this_hash[k] : cnt += v
      end
    end
    cnt - exact_matches(other_code)
  end #near_matches

  def ==(other_code)
    exact_matches(other_code) == pegs.length
  end #[]
end #class Code

class Game
  attr_reader :secret_code

  def initialize(code=Code.random)
    @secret_code = code
  end #initialize

  def get_guess
    input = gets.chomp
    Code.parse(input)
  end #get_guess

  def display_matches(code)
    puts "exact matches: #{secret_code.exact_matches(code)}, near matches: #{secret_code.near_matches(code)}"
  end #display_matches
end #class Game
